(ns plf02.core)

(defn funcion-associative?-1 
  [a]
  (associative? a))
(defn funcion-associative-2
  [b]
  (associative? b))
(defn funcion-associative?-3
  [a b c]
  [(associative? a) (associative? b) (associative? c)])

(funcion-associative?-1 [1 2 3 4])
(funcion-associative-2  #{1 2 3})
(funcion-associative?-3 [10 20 30] {:saludo "hola"} '(1 2 3))

(defn funcion-boolean?-1
  [a]
  (boolean? a))
(defn funcion-boolean?-2
  [b]
  (boolean? b))
(defn funcion-boolean?-3
  [c]
  (boolean? c))


(funcion-boolean?-1 true)
(funcion-boolean?-2 (neg? -1))
(funcion-boolean?-3 (number? \a))


(defn funcion-char?-1
  [a]
  (char? a))
 (defn funcion-char?-2
   [b]
   (char? b))
(defn funcion-char?-3
  [c]
  (char? c))

(funcion-char?-1 \a)
(funcion-char?-2 1)
(funcion-char?-3 [\a \b 1 2 3 4 \c])


(defn funcion-coll?-1
  [a]
  (coll? a))
(defn funcion-coll?-2
  [b]
  (coll? b))
(defn funcion-coll?-3
  [c]
   (coll? c))
 
(funcion-coll?-1 [1 2 3])
(funcion-coll?-2 [#{1 2 3} {:sal :pimienta} \a])
(funcion-coll?-3 [#{} [] 1 2 3] )

(defn funcion-decimal?-1
  [a]
  (decimal? a))
(defn funcion-decimal?-2
  [b]
  ( decimal? b))
(defn funcion-decimal?-3
  [c]
  (decimal? c))

(funcion-decimal?-1 1.0M)
(funcion-decimal?-2 3)
(funcion-decimal?-3 \a)
 
(defn funcion-double?-1
  [a]
  (double? a))
(defn funcion-double?-2
  [b]
  (double? b))
(defn funcion-double?-3
  [c]
  (double? c))

(funcion-double?-1 1.0)
(funcion-double?-2 1.0M)
(funcion-double?-3 7.04)

(defn funcion-float?-1
  [a]
  (float? a))
(defn funcion-float?-2
  [b]
  (float? b))
(defn funcion-float?-3
  [c]
  (float? c))

(funcion-float?-1 1.0)
(funcion-float?-2 [1.0M 2.0M 3.0 4.1 5.2])
(funcion-float?-3 2.2)

(defn funcion-ident?-1
  [a]
  (ident? a))

(defn funcion-ident?-2
  [b]
  (ident? b))

(defn funcion-ident?-3
  [c]
  (ident? c))


(funcion-ident?-1 :A)
(funcion-ident?-2 :B)
(funcion-ident?-3 :C) 

(defn funcion-indexed?-1
  [a]
  (indexed? a))
(defn funcion-indexed?-2
  [b]
  (indexed? b))

(defn funcion-indexed?-3
  [c]
  (indexed? c))

(funcion-indexed?-1 [1 2 3])
(funcion-indexed?-2 #{3 4})
(funcion-indexed?-3 '(1 2 3))


(defn funcion-int?-1
  [a]
  (int? a))

(defn funcion-int?-2
  [b]
  (int? b))
(defn funcion-int?-3
  [c]
  (int? c))


(funcion-int?-1 1)
(funcion-int?-2 (max 2 3))
(funcion-int?-3 (min 1 0))

(defn funcion-integer?-1
  [a]
  (integer? a))

(defn funcion-integer?-2
  [b]
  (integer? b))
(defn funcion-integer?-3
  [c]
  (integer? c))


(funcion-integer?-1 1)
(funcion-integer?-2 (max 1 2 3))
(funcion-integer?-3 (min 10 2))

(defn funcion-keyword?-1
  [a]
  (keyword? a))

(defn funcion-keyword?-2
  [b]
  (keyword? b))
(defn funcion-keyword?-3
  [c]
  (keyword? c))

(funcion-keyword?-1 :hola)
(funcion-keyword?-2 :adios)
(funcion-keyword?-3 :saludos)

(defn funcion-list?-1
  [a]
  (list? a))

(defn funcion-list?-2
  [b]
  (list? b))
(defn funcion-list?-3
  [c]
  (list? c))

(funcion-list?-1 '(1 2 3))
(funcion-list?-2 (range 5 10))
(funcion-list?-3 (list 2 4))

(defn funcion-map-entry-1
  [a]
  (map-entry? a))

(defn funcion-map-entry-2
  [b]
  (map-entry? b))
(defn funcion-map-entry-3
  [c]
  (map-entry? c ))

(funcion-map-entry-1 (first {:uno 1}))
(funcion-map-entry-2 (first {:dos 2}))
(funcion-map-entry-3 (first {:tres 3}) )

(defn funcion-map?-1
  [a]
  (map? a))

(defn funcion-map?-2
  [b]
  (map? b))
(defn funcion-map?-3
  [c]
  (map? c))

(funcion-map?-1 {:uno 1})
(funcion-map?-2 {:dos 2})
(funcion-map?-3 {:tres 3})

(defn funcion-nat-int?-1
  [a]
  (nat-int? a))

(defn funcion-nat-int?-2
  [b]
  (nat-int? b))
(defn funcion-nat-int?-3
  [c]
  (nat-int? c))

(funcion-nat-int?-1 10)
(funcion-nat-int?-2 (min -1 -3))
(funcion-nat-int?-3 (max 0 1))

(defn funcion-number?-1
  [a]
  (number? a))

(defn funcion-number?-2
  [b]
  (number? b))
(defn funcion-number?-3
  [c]
  (number? c))

(funcion-number?-1 1)
(funcion-number?-2 3.0M)
(funcion-number?-3 1.0) 

(defn funcion-pos-int?-1
  [a]
  (pos-int? a))
(defn funcion-pos-int?-2
  [b]
  (pos-int? b))
(defn funcion-pos-int?-3
  [c]
  (pos-int? c))

(funcion-pos-int?-1 1)
(funcion-pos-int?-2 (max 10 12))
(funcion-pos-int?-3 (min 1 0))

(defn funcion-ratio?-1
  [a]
  (ratio? a))
(defn funcion-ratio?-2
  [b]
  (ratio? b))
(defn funcion-ratio?-3
  [c]
  (ratio? c))

(funcion-ratio?-1 1/4)
(funcion-ratio?-2 2/5)
(funcion-ratio?-3 3/6)

(defn funcion-rational?-1
  [a]
  (rational? a))
(defn funcion-rational?-2
  [b]
  (rational? b))
(defn funcion-rational?-3
  [c]
  (rational? c))

(funcion-rational?-1 1/4)
(funcion-rational?-2 2/5)
(funcion-rational?-3 3/6)

(defn funcion-seq?-1
  [a]
  (seq? a))
(defn funcion-seq?-2
  [b]
  (seq? b))

(defn funcion-seq?-3
  [c]
  (seq? c))

(funcion-seq?-1 (seq [1 2]))
(funcion-seq?-2 (range 1 9))
(funcion-seq?-3 (range 1 9 3))

(defn funcion-sequential?-1
  [a]
  (sequential? a))
(defn funcion-sequential?-2
  [b]
  (sequential? b))
(defn funcion-sequential?-3
  [c]
  (sequential? c))

(funcion-sequential?-1 [1 2 3])
(funcion-sequential?-2 [[1 2] '(3 4) #{5 6}])
(funcion-sequential?-3 '(1 2))

(defn funcion-set?-1
  [a]
  (set? a))

(defn funcion-set?-2
  [b]
  (set? b))
(defn funcion-set?-3
  [c]
  (set? c))


(funcion-set?-1 #{1 2})
(funcion-set?-2 [[1 2] '(3 4) #{4 5} {:5 :6}])
(funcion-set?-3 #{9 10})

(defn funcion-some?-1
  [a]
  (some? a))
(defn funcion-some?-2
  [b]
  (some? b))
(defn funcion-some?-3
  [c]
  (some? c))

(funcion-some?-1 1)
(funcion-some?-2 [nil nil 1])
(funcion-some?-3 nil)

(defn funcion-string?-1
  [a]
  (string? a))
(defn funcion-string?-2
  [b]
  (string? b))
(defn funcion-string?-3
  [c]
  (string? c))

(funcion-string?-1 "abc")
(funcion-string?-2 "def")
(funcion-string?-3 "ghi")

(defn funcion-symbol?-1
  [a]
  (symbol? a))
(defn funcion-symbol?-2
  [b]
  (and symbol? b))
(defn funcion-symbol?-3
  [c]
  (symbol? c))

(funcion-symbol?-1 'a)
(funcion-symbol?-2 'b)
(funcion-symbol?-3 'c)

(defn funcion-vector?-1
  [a]
  (vector? a))
(defn funcion-vector?-2
  [b]
  (vector? b))
(defn funcion-vector?-3
  [c]
  (vector? c))

(funcion-vector?-1 [1 2 3])
(funcion-vector?-2 [[12 14] '(1 2) #{5 6} [1]])
(funcion-vector?-3 [[12 14] '(1 2) #{5 6} [1]])

(defn funcion-drop-1
  [a b]
  (drop a b))
(defn funcion-drop-2
  [a b]
  (drop a b))
(defn funcion-drop-3
  [a b c]
  (drop a (range b c)))

(funcion-drop-1 2 [1 2 3 4 5])
(funcion-drop-2 1 [[1 2 3] [4 5 6] [7 8] [9 10]])
(funcion-drop-3 2 5 11)

(defn funcion-drop-last-1
   [a]
  (drop-last a))
(defn funcion-drop-last-2
  [a b]
  (drop-last a b))
(defn funcion-drop-last-3
  [a b c]
  (drop-last a (range b  c)))

(funcion-drop-last-1 [10 20 30 40])
(funcion-drop-last-2 2 [10 20 30 40])
(funcion-drop-last-3 2 5 26)

(defn funcion-drop-while-1
  [a]
  (drop-while pos? a))

(defn funcion-drop-while-2
  [a]
  (drop-while neg? a))
(defn funcion-drop-while-3
  [a]
  (drop-while zero? a))

(funcion-drop-while-1 [1 2 3 4 -1 -2])
(funcion-drop-while-2 [-1 -2 1 2 3 4])
(funcion-drop-while-3 [-1 -2 1 2 3 4])

(defn funcion-every?-1
  [a]
  (every? coll? a))
(defn funcion-every?-2
  [a]
  (every? vector? a))
(defn funcion-every?-3
  [a]
  (every? number? a))

(funcion-every?-1 ['() #{} {} []])
(funcion-every?-2[ [1 2 3]])
(funcion-every?-3 [1 2 3 4 5 6])

(defn funcion-filterv-1
  [a]
  (filterv char? a))
(defn funcion-filterv-2
  [a]
  (filterv double? a))
(defn funcion-filterv-3
  [a]
  (filterv boolean? a))

(funcion-filterv-1 [1 2 \a \b])
(funcion-filterv-2 [1.1 2.1 3 4])
(funcion-filterv-3 [true false 1 2])

(defn funcion-group-by-1
 [a]
 (group-by count a))
(defn funcion-group-by-2
  [a]
  (group-by number? a))
(defn funcion-group-by-3
  [a]
  (group-by coll? a))

(funcion-group-by-1 ["a" "b" "C" "aa"])
(funcion-group-by-2 [1 2 3 4 \c \d])
(funcion-group-by-3 [[5 10] [6 12] #{1 2 3} {:uno 1}])

(defn funcion-keep-1
  [a]
  (keep neg? a))
(defn funcion-keep-2
  [a]
  (keep pos? a))
(defn funcion-keep-3
  [a]
  (keep decimal? a))

(funcion-keep-1 [-1 -2 3])
(funcion-keep-2 [-1 -2 3])
(funcion-keep-3 [-1 -2 3])

(defn funcion-keep-index-1
  [a]
  (keep-indexed #(if (number? %1) %2) a))

(defn funcion-keep-index-2
  [a]
  (keep-indexed #(if (number? %2) %1) a))

(defn funcion-keep-index-3
  [a]
  (keep-indexed #(if (char? %2) %1) a))

(funcion-keep-index-1 [10 20 30 40])
(funcion-keep-index-2 [10 20 30 40])
(funcion-keep-index-3 [\a \b \c 1])

(defn funcion-map-indexed-1
  [a]
  (map-indexed vector a))

(defn funcion-map-indexed-2
  [a b]
  (map-indexed vector (concat a b)))

(defn funcion-map-indexed-3
  [a]
  (map-indexed hash-map a))

(funcion-map-indexed-1 "amigos")
(funcion-map-indexed-2 "hola" [\t \o \d \o \s])
(funcion-map-indexed-3 "novia")

(defn funcion-mapcat-1
  [a]
  (mapcat list a))
(defn funcion-mapcat-2
  [a b]
  (mapcat list a b))
(defn funcion-mapcat-3
  [a]
  (mapcat vector a))

(funcion-mapcat-1 "aprobado")
(funcion-mapcat-2 "reprobado" "NA")
(funcion-mapcat-3 "tierra")

(defn funcion-mapv-1
  [a b]
  (mapv * a b))
(defn funcion-mapv-2
  [a b]
  (mapv - a b))
(defn funcion-mapv-3
  [a]
  (mapv inc a))

(funcion-mapv-1 [2 3 4][10 11 12])
(funcion-mapv-2 [2 2] [3 4])
(funcion-mapv-3 [1 2 3 4 5 6 7 8])

(defn funcion-merge-wiht-1
  [a b]
  (merge-with + a b))

(defn funcion-merge-wiht-2
  [a b]
  (merge-with into a b))

(defn funcion-merge-wiht-3
  [a b]
  (merge-with * a b))

(funcion-merge-wiht-1 {:uno 1 :dos 2} {:uno 10 :dos 20})
(funcion-merge-wiht-2 {"saludos" ["hola" "hi"] "despedidad" ["adios" "byw"]} {"saludos" ["hello"] "despedidad" ["goodbye"]})
(funcion-merge-wiht-3 {:uno 1 :dos 2} {:uno 10 :dos 20})

(defn funcion-not-any?-1
  [a]
  (not-any? char? a))

(defn funcion-not-any?-2
  [a]
  (not-any? number? a))

(defn funcion-not-any?-3
  [a]
  (not-any? coll? a))

(funcion-not-any?-1 [1 2 3])
(funcion-not-any?-2 "hola")
(funcion-not-any?-2 [1])

(defn funcion-not-every?-1
  [a]
  (not-every? char? a))

(defn funcion-not-every?-2
  [a]
  (not-every? number? a))

(defn funcion-not-every?-3
  [a]
  (not-every? coll? a))

(funcion-not-every?-1 [1 2 3])
(funcion-not-every?-2 "hola")
(funcion-not-every?-2 [1])

(defn funcion-partition-by-1
  [a]
  (partition-by count a))

(defn funcion-partition-by-2
  [a]
  (partition-by identity a))
(defn funcion-partition-by-3
  [a]
  (partition-by string? a))

(funcion-partition-by-1 ["a" "a" "bd"])
(funcion-partition-by-2 [1 2 1 3 3 3])
(funcion-partition-by-3 ["hola" \a \b])

(defn funcion-reduce-kv-1 
  [m f]
  (reduce-kv (fn [m k v]
               (assoc m k (f v))) {} m))

(defn funcion-reduce-kv-2
  [a]
  (reduce-kv #(assoc %1 %2 %3) {} a))

(defn funcion-reduce-kv-3
  [a]
  (reduce-kv (fn [m k v]
               (if (empty? v) m (assoc m (keyword v) (name k)))) {} a))


(funcion-reduce-kv-1 1 2)
(funcion-reduce-kv-2 {:a 1 :b 2 :c 3 :d 4})
(funcion-reduce-kv-3 {:foo "food", :bar "barista", :baz "bazaar"})

(defn funcion-remove-1
  [a]
  (remove number? a))
(defn funcion-remove-2
  [a]
  (remove string? a))
(defn funcion-remove-3
  [a]
  (remove boolean? a))

(funcion-remove-1 [1 2 3 \h \i])
(funcion-remove-2 ["ab" "cd" true false])
(funcion-remove-3 ["ab" "cd" true false])

(defn funcion-reverse-1
  [a]
  (reverse a))
(defn funcion-reverse-2
  [a]
  (apply list (reverse a)))
(defn funcion-reverse-3
  [a]
  (apply vector (reverse a)))

(funcion-reverse-1 [10 20])
(funcion-reverse-2 [15 25 30])
(funcion-reverse-3 [2 4 8 12])

(defn funcion-some-1
  [a]
  (some pos? a))

(defn funcion-some-2
  [a]
  (some neg? a))

(defn funcion-some-3
  [a]
  (some true? a))

(funcion-some-1 [1 2 3])
(funcion-some-2 [-1 2 3])
(funcion-some-3 [true false])

(defn funcion-sort-by-1
  [a]
  (sort-by max a))

(defn funcion-sort-by-2
  [a]
  (sort-by pos? a))

(defn funcion-sort-by-3
  [a]
  (sort-by neg? a))

(funcion-sort-by-1 [2 8 7 5])
(funcion-sort-by-2 [2 -2 1 -1 10 2])
(funcion-sort-by-3 [2 -2 1 -1 10 2])

(defn funcion-split-with-1
  [a]
  (split-with char? a))
(defn funcion-split-with-2
  [a]
  (split-with (partial > 1) a))
(defn funcion-split-with-3
  [a]
  (split-with boolean? a))

(funcion-split-with-1 [\a \b 1 2])
(funcion-split-with-2 [0 1 2 4 8])
(funcion-split-with-3 [true false 3 2])

(defn funcion-take-1
  [a b]
  (take a b))
(defn funcion-take-2
  [a b c]
  (take a (range b c)))
(defn funcion-take-3
  [a b c d]
  (take a (range b c d)))

(funcion-take-1 2 [10 20 300 4000])
(funcion-take-2 3 10 100)
(funcion-take-3 4 10 100 5)

(defn funcion-take-last-1
  [a b]
  (take-last a b))
(defn funcion-take-last-2
  [a b c]
  (take-last a (range b c)))
(defn funcion-take-last-3
  [a b c d]
  (take-last a (range b c d)))

(funcion-take-last-1 2 [10 20 300 4000])
(funcion-take-last-2 3 10 100)
(funcion-take-last-3 4 10 100 5)

(defn funcion-take-nth-1
  [a b]
  (take-nth a b))
(defn funcion-take-nth-2
  [a b c]
  (take-nth a (range b c)))
(defn funcion-take-nth-3
  [a b c d]
  (take-nth a (range b c d)))

(funcion-take-nth-1 2 [10 20 300 4000])
(funcion-take-nth-2 3 10 100)
(funcion-take-nth-3 4 10 100 5)

(defn funcion-take-while-1
  [a]
  (take-while pos? a))
(defn funcion-take-while-2
  [a]
  (take-while number? a))
(defn funcion-take-while-3
  [a]
  (take-while coll? a))

(funcion-take-while-1 [1 2 0 -1 -2])
(funcion-take-while-2 [1 2 \a \b])
(funcion-take-while-3 [{} #{1 2 3} '(4 5) 7 8 9])

(defn funcion-update-1
  [a b]
(update a b inc))

(defn funcion-update-2
  [a b]
  (update a b inc))
(defn funcion-update-3
  [a b]
  (update a b dec))

(funcion-update-1 {:uno 1} :uno)
(funcion-update-2 [1 2 3] 1)
(funcion-update-3 [-4 -5 -6] 2)

(defn funcion-update-in-1
  [a b c]
  (update-in a b c))
(defn funcion-update-in-2
  [a b]
  (update-in a b inc))
(defn funcion-update-in-3
  [a b]
  (update-in a b dec))

(funcion-update-in-1 0 [1 2 3] 10)
(funcion-update-2 [1 2 3] 1)
(funcion-update-3 [-4 -5 -6] 2)





























